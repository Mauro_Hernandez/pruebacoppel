package com.pruebacoppel.pruebacoppel.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Hero(
    var id: String?,
    var name: String?,
    var thumbnail: Thumbnail?,
    var comics: Comics?,
    var series: Series?,
    var stories: Stories?
):Parcelable