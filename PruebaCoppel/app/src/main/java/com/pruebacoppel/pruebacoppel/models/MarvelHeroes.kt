package com.pruebacoppel.pruebacoppel.models


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MarvelHeroes(
    var status: String? = "",
    var `data`: Data?
):Parcelable