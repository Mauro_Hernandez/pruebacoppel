package com.pruebacoppel.pruebacoppel.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Stories(
       var available: Int,
       var items: ArrayList<Items>?= ArrayList()
): Parcelable

