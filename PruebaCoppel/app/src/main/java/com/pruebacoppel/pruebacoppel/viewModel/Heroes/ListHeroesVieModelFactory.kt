package com.pruebacoppel.pruebacoppel.viewModel.Heroes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pruebacoppel.pruebacoppel.domain.Heroes.ListHeroesUseCase

class ListHeroesVieModelFactory(val listHeroesUsecase: ListHeroesUseCase):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(ListHeroesUseCase::class.java).newInstance(listHeroesUsecase)
    }
}