package com.pruebacoppel.pruebacoppel.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Comics(
    var available: Int,
    var items: ArrayList<Items>?= ArrayList()
): Parcelable