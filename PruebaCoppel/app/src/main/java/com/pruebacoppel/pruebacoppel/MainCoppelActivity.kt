package com.pruebacoppel.pruebacoppel

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.pruebacoppel.pruebacoppel.utils.NetworkConnetion
import kotlinx.android.synthetic.main.activity_main_heroes.*

class MainCoppelActivity : AppCompatActivity() {


    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_heroes)
        navController= findNavController(R.id.nav_host_fragment)
        val networkConnection = NetworkConnetion(this)
        networkConnection.observe(this, Observer { isConnected ->
            isConnected?.let {
                if(it){
                    linear_NoInternet.visibility= View.GONE
                }else{
                    linear_NoInternet.visibility= View.VISIBLE
                }
            }
        })

    }

}
