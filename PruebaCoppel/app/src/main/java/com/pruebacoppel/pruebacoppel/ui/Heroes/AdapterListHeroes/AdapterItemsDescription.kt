package com.pruebacoppel.pruebacoppel.ui.Heroes.AdapterListHeroes

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pruebacoppel.pruebacoppel.R
import com.pruebacoppel.pruebacoppel.models.Items
import kotlinx.android.synthetic.main.item_recycler.view.*

class AdapterItemsDescription(listData:ArrayList<Items>): RecyclerView.Adapter<AdapterItemsDescription.AdapterViewHolderItem>(){

    var listItems:ArrayList<Items>?=listData
    var viewHolder:AdapterViewHolderItem?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolderItem {
        val v= LayoutInflater.from(parent?.context).inflate(R.layout.item_recycler,parent,false)
        viewHolder= AdapterViewHolderItem(v)
        return viewHolder!!
    }

    override fun onBindViewHolder(holder: AdapterViewHolderItem, position: Int) {
        Log.e("list", "item:${listItems!![position].name!!} " )
        holder.description.text= listItems!![position].name!!
    }

    override fun getItemCount(): Int {
        return listItems!!.size
    }

    class AdapterViewHolderItem(view: View): RecyclerView.ViewHolder(view!!){
        val description=view.tv_item
    }
}