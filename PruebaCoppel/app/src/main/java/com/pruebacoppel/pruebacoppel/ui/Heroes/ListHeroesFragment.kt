package com.pruebacoppel.pruebacoppel.ui.Heroes

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pruebacoppel.pruebacoppel.Constant.VIEW_TYPE_ITEM
import com.pruebacoppel.pruebacoppel.Constant.VIEW_TYPE_LOADING
import com.pruebacoppel.pruebacoppel.R
import com.pruebacoppel.pruebacoppel.domain.Heroes.ListHeroesUseCase
import com.pruebacoppel.pruebacoppel.models.Data
import com.pruebacoppel.pruebacoppel.models.Hero
import com.pruebacoppel.pruebacoppel.ui.Heroes.AdapterListHeroes.AdapterCardviewListHeroes
import com.pruebacoppel.pruebacoppel.utils.CustomProgressBar
import com.pruebacoppel.pruebacoppel.utils.OnLoadMoreListener
import com.pruebacoppel.pruebacoppel.utils.RecyclerViewLoadMoreScroll
import com.pruebacoppel.pruebacoppel.viewModel.Heroes.ListHeroesVieModelFactory
import com.pruebacoppel.pruebacoppel.viewModel.Heroes.ListHeroesViewModel

class ListHeroesFragment : Fragment() {
    private lateinit var viewModel: ListHeroesViewModel
    lateinit var itemsHeroes: ArrayList<Hero>
    lateinit var loadMoreItemsCells: ArrayList<Hero>
    private var recyclerView: RecyclerView?=null
    lateinit var navController: NavController
    private var offset:Int =0
    val progressBar = CustomProgressBar()
    lateinit var adapterGrid:AdapterCardviewListHeroes

    lateinit var scrollListener: RecyclerViewLoadMoreScroll
    lateinit var mLayoutManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_list_heroes, container, false)
        recyclerView=view.findViewById(R.id.rv_articles_listArticles)
        setupInicioFragentViewModel()
        setItemsData()

        return view!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController= Navigation.findNavController(view)
        setAdapter()
        setRVLayoutManager()
        setRVScrollListener()
    }

    fun setupInicioFragentViewModel(){
        viewModel= ViewModelProviders.of(this, ListHeroesVieModelFactory(ListHeroesUseCase())).get(
            ListHeroesViewModel::class.java)
        viewModel.attachView(this)
    }

    private fun setItemsData() {
        itemsHeroes= ArrayList()
        viewModel?.getListHeroes(0)
        viewModel?.listHeroes.observe(this.viewLifecycleOwner, Observer { heroes->
            when(offset){
                0 -> {
                    this.itemsHeroes=heroes!!
                    setAdapter()
                }
                else -> {
                    this.loadMoreItemsCells=heroes!!
                    updateRecyclerView()
                }
            }

        })
    }

    private fun setAdapter() {
        adapterGrid = AdapterCardviewListHeroes(activity!!.applicationContext,itemsHeroes!!,this)
        adapterGrid.notifyDataSetChanged()
        recyclerView?.adapter = adapterGrid
    }

    private fun setRVLayoutManager() {
        mLayoutManager = GridLayoutManager(activity, 2)
        recyclerView?.layoutManager = mLayoutManager
        recyclerView?.setHasFixedSize(true)
        recyclerView?.adapter = adapterGrid
        (mLayoutManager as GridLayoutManager).spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (adapterGrid.getItemViewType(position)) {
                    VIEW_TYPE_ITEM -> 1
                    VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                    else -> -1
                }
            }
        }
    }

    private fun setRVScrollListener() {
        scrollListener = RecyclerViewLoadMoreScroll(mLayoutManager as GridLayoutManager)
        scrollListener.setOnLoadMoreListener(object :
            OnLoadMoreListener {
            override fun onLoadMore() {
                LoadMoreData()
            }
        })

        recyclerView?.addOnScrollListener(scrollListener)
    }

    private fun LoadMoreData() {
        loadMoreItemsCells = ArrayList()
        offset = adapterGrid.itemCount
        viewModel?.getListHeroes(offset+1)
    }


    fun showProgressBar(){
        progressBar.show(activity!!," ")
    }
    fun hideProgressBar(){
        progressBar.dialog.dismiss()
    }

    fun updateRecyclerView(){
        adapterGrid.addData(loadMoreItemsCells)
        adapterGrid.notifyDataSetChanged()
        scrollListener.setLoaded()
    }

    override fun onDetach() {
        super.onDetach()
        viewModel.dettachJob()
        viewModel.dettachView()
    }

    fun showException(message:String){
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()

    }

    fun showMessageHero(message:String){
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    fun goHero(hero : Hero){
        val articleAux= ListHeroesFragmentDirections.actionListArticlesFragmentToArticleFragment(hero)
            navController.navigate(articleAux!!)
    }




}
