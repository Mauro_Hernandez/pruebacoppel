package com.pruebacoppel.pruebacoppel.models


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Data(
    var results: ArrayList<Hero>? = arrayListOf()

):Parcelable