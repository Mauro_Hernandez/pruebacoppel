package com.pruebacoppel.pruebacoppel.utils

interface OnLoadMoreListener {
    fun onLoadMore()
}