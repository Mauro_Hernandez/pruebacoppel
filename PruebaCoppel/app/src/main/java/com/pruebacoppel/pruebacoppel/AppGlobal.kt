package com.pruebacoppel.pruebacoppel

import android.app.Application
import android.content.Context

class AppGlobal():Application() {
    companion object {
        lateinit var CONTEXT:Context
    }

    override fun onCreate() {
        super.onCreate()
        CONTEXT =applicationContext
    }
}