package com.pruebacoppel.pruebacoppel.ui.Heroes


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.pruebacoppel.pruebacoppel.R
import com.pruebacoppel.pruebacoppel.models.Data
import com.pruebacoppel.pruebacoppel.models.Hero
import com.pruebacoppel.pruebacoppel.ui.Heroes.AdapterListHeroes.AdapterItemsDescription
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_hero.*

class HeroFragment : Fragment() {
    lateinit var hero: Hero
    lateinit var navController: NavController
    private var rv_comics: RecyclerView?=null
    private var rv_series: RecyclerView?=null
    private var rv_stories: RecyclerView?=null
    var visibleComics:Boolean=false
    var visibleSeries:Boolean=false
    var visibleStories:Boolean=false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v=inflater.inflate(R.layout.fragment_hero, container, false)
        rv_comics=v?.findViewById(R.id.rv_comics)
        rv_series=v?.findViewById(R.id.rv_series)
        rv_stories=v?.findViewById(R.id.rv_stories)
        return v!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController= Navigation.findNavController(view)
        hero= HeroFragmentArgs.fromBundle(arguments!!).hero!!
        setupViewsArticle(hero)
        setupButtons()
        setupRV()
    }

    fun setupViewsArticle( hero: Hero){

        Picasso.get()
            .load(hero.thumbnail!!.path+"."+hero.thumbnail!!.extension)
            .error(R.drawable.imagen_no_disponible_)
            .into(img_foto_Article)

        tv_name.text= this.hero.name
        tv_num_comics.text="Comics Disponibles: ${this.hero!!.comics!!.available}"
        tv_num_series.text= "Series Disponibles: ${this.hero!!.series!!.available}"
        tv_num_stories.text= "Cuentos Disponibles: ${this.hero!!.stories!!.available}"
    }

    fun setupRV(){

        var adapter_comics= AdapterItemsDescription(this.hero.comics?.items!!)

        rv_comics?.layoutManager= LinearLayoutManager(activity)
        rv_comics?.adapter=adapter_comics

        var adapter_series= AdapterItemsDescription(this.hero.series!!.items!!)
        rv_stories?.layoutManager= LinearLayoutManager(activity)
        rv_stories?.adapter=adapter_series

        var adapter_stories= AdapterItemsDescription(this.hero.stories?.items!!)
        rv_series?.layoutManager= LinearLayoutManager(activity)
        rv_series?.adapter=adapter_stories

    }


    fun setupButtons(){
        btn_back_Article.setOnClickListener {
            activity!!.onBackPressed()
        }

        btn_more_comics.setOnClickListener {
            if (this.hero.comics?.available!! >0 && !visibleComics){
                ll_comics.visibility=View.VISIBLE
                visibleComics=true
            }
            else if(this.hero.comics?.available!! >0 && visibleComics){
                ll_comics.visibility=View.GONE
                visibleComics=false
            }

        }
        btn_more_series.setOnClickListener {
            if (this.hero.series?.available!! >0 && !visibleSeries){
                ll_series.visibility=View.VISIBLE
                visibleSeries=true
            }
            else if(this.hero.series?.available!! >0 && visibleSeries){
                ll_series.visibility=View.GONE
                visibleSeries=false
            }

        }
        btn_more_stories.setOnClickListener {
            if (this.hero.stories?.available!! >0 && !visibleStories){
                ll_stories.visibility=View.VISIBLE
                visibleStories=true
            }
            else if(this.hero.stories?.available!! >0 && visibleStories){
                ll_stories.visibility=View.GONE
                visibleStories=false
            }
        }
    }




}
