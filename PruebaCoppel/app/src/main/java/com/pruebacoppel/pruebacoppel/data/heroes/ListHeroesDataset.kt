package com.pruebacoppel.pruebacoppel.data.heroes

import android.util.Log
import com.pruebacoppel.pruebacoppel.Constant.BASE_URL_API_HASH
import com.pruebacoppel.pruebacoppel.Constant.BASE_URL_API_KEY
import com.pruebacoppel.pruebacoppel.Constant.BASE_URL_API_MARVEL
import com.pruebacoppel.pruebacoppel.Constant.BASE_URL_API_TS
import com.pruebacoppel.pruebacoppel.exceptions.Exceptions
import com.pruebacoppel.pruebacoppel.models.Hero
import com.pruebacoppel.pruebacoppel.models.MarvelHeroes
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class ListHeroesDataset {

    lateinit var service: ApiserviceListHeroes
    var marvelHeroes: MarvelHeroes?=null
    var hero: Hero?=null

    fun setupServive(){
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL_API_MARVEL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create<ApiserviceListHeroes>(ApiserviceListHeroes::class.java)

    }

    suspend fun searchHeroes(offset: Int):MarvelHeroes= suspendCancellableCoroutine{ continuar->
        setupServive()
            service.getHeroes(BASE_URL_API_TS, BASE_URL_API_KEY, BASE_URL_API_HASH,offset).enqueue(
                object : Callback<MarvelHeroes> {
                    override fun onResponse(
                        call: Call<MarvelHeroes>,
                        response: Response<MarvelHeroes>
                    ) {
                        marvelHeroes = response.body()
                        Log.e("retrofit", "hero ${marvelHeroes!!.data!!.results!!.size} " )
                        continuar.resume(marvelHeroes!!)
                    }

                    override fun onFailure(call: Call<MarvelHeroes>, t: Throwable) {
                        Log.e("retrofit", "error ${t.message.toString()} " )
                        continuar.resumeWithException(Exceptions(t.message.toString()))
                    }
                })



    }

}