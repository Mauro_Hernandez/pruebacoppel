package com.pruebacoppel.pruebacoppel

object Constant {

    const val BASE_URL_API_MARVEL = "https://gateway.marvel.com/"
    const val BASE_URL_API_CHARACTERS = "v1/public/characters"
    const val BASE_URL_API_TS = "5"
    const val BASE_URL_API_KEY = "60f18e46a1b2de0ec96ff87e6be4aae8"
    const val BASE_URL_API_HASH = "216bc052e80aaadbe35e6e35324e0dc9"
    const val VIEW_TYPE_ITEM = 0
    const val VIEW_TYPE_LOADING = 1
}