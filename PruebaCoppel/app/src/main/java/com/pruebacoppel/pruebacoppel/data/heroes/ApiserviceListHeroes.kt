package com.pruebacoppel.pruebacoppel.data.heroes

import com.pruebacoppel.pruebacoppel.Constant.BASE_URL_API_CHARACTERS
import com.pruebacoppel.pruebacoppel.models.MarvelHeroes
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiserviceListHeroes {

    @GET(BASE_URL_API_CHARACTERS)
    fun getHeroes(@Query("ts") ts : String,@Query("apikey") apiKey : String,@Query("hash") hash : String,@Query("offset")offset : Int):Call<MarvelHeroes>
}