package com.pruebacoppel.pruebacoppel.ui.Heroes.AdapterListHeroes

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pruebacoppel.pruebacoppel.R
import com.pruebacoppel.pruebacoppel.models.Hero
import com.pruebacoppel.pruebacoppel.ui.Heroes.ListHeroesFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cardview_article.view.*

class AdapterCardviewListHeroes(var context: Context, listData:ArrayList<Hero>, viewH: ListHeroesFragment): RecyclerView.Adapter<AdapterCardviewListHeroes.AdapterViewHolder>() {
    var listData:ArrayList<Hero>?=listData
    var viewHolder: AdapterViewHolder?=null
    var viewHero:ListHeroesFragment?= viewH

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        val v=LayoutInflater.from(parent?.context).inflate(R.layout.cardview_article,parent,false)
        viewHolder= AdapterViewHolder(v)
        return viewHolder!!

    }

    override fun getItemCount(): Int {
        return listData?.size!!
    }

    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {
        holder.description.text=listData?.get(position)?.name.toString()

        Picasso
            .get()
            .load(listData?.get(position)?.thumbnail?.path!!+"."+listData?.get(position)?.thumbnail?.extension!!)
            .error(R.drawable.imagen_no_disponible_)
            .into(holder.img)

        holder.img.setOnClickListener {
          viewHero?.goHero(listData?.get(position)!!)
        }
        holder.description.setOnClickListener {
            viewHero?.goHero(listData?.get(position)!!)
        }
    }

    fun addData(dataViews: ArrayList<Hero>) {
        this.listData!!.addAll(dataViews)
        notifyDataSetChanged()
    }

    fun getItemAtPosition(position: Int): Hero? {
        return listData!![position]
    }


    fun removeLoadingView() {
        //Remove loading item
        if (listData!!.size != 0) {
            listData!!.removeAt(listData!!.size - 1)
            notifyItemRemoved(listData!!.size)
        }
    }


    class AdapterViewHolder(view:View):RecyclerView.ViewHolder(view!!){
        val img=view.cv_img
        val description=view.tv_Description_Card
    }
}