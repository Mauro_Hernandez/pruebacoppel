package com.pruebacoppel.pruebacoppel.viewModel.Heroes

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pruebacoppel.pruebacoppel.domain.Heroes.ListHeroesUseCase
import com.pruebacoppel.pruebacoppel.exceptions.Exceptions
import com.pruebacoppel.pruebacoppel.models.Hero
import com.pruebacoppel.pruebacoppel.ui.Heroes.ListHeroesFragment
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class ListHeroesViewModel(val listHeroesUseCase: ListHeroesUseCase):ViewModel(), CoroutineScope {
    private var view: ListHeroesFragment?=null
    val listHeroes= MutableLiveData<ArrayList<Hero>>()
    val job=Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main+job


    fun getListHeroes(offset: Int){
        launch{
            try{
                view?.showProgressBar()
                setListHeroes(listHeroesUseCase.getHeroes(offset).data?.results!!)
                view?.hideProgressBar()
            }catch (e: Exceptions){
                view?.showException(e.message.toString())
                view?.hideProgressBar()
            }
        }

    }
    fun setListHeroes(heroes:ArrayList<Hero>){
        listHeroes.value=heroes
    }

    fun attachView(view: ListHeroesFragment){
        this.view=view
    }
    fun dettachView(){
        this.view=null
    }

    fun dettachJob(){
        coroutineContext.cancel()
    }
}