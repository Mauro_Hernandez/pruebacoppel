package com.pruebacoppel.pruebacoppel.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Thumbnail(
    var path: String="",
    var extension: String =".jpg"
):Parcelable