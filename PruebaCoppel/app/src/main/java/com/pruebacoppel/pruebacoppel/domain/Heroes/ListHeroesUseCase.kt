package com.pruebacoppel.pruebacoppel.domain.Heroes

import com.pruebacoppel.pruebacoppel.data.heroes.ListHeroesDataset
import com.pruebacoppel.pruebacoppel.models.MarvelHeroes

class ListHeroesUseCase {
    private val heroesDataset= ListHeroesDataset()

    suspend fun getHeroes(offset: Int):MarvelHeroes{
        val heroData= heroesDataset.searchHeroes(offset)
       return heroData
    }


}